<div class="card-header">
<h4><?php echo $title; ?></h4>
</div>
<div class="card-body">
<?php echo validation_errors(); ?>
<div style="color: red;"><?php echo (isset($message))? $message : ""; ?></div>
<?php echo form_open_multipart('news/user_create'); ?>
	<input type="hidden" name="author" value="<?php echo $user_item['username']; ?>"/>
	
	<div class="form-group">
	    <label for="title">News Title</label>
	    <input class="form-control" type="input" name="title" required/>
   	</div>

   	<div class="form-group">
	    <label for="text">News Text</label>
	    <textarea id="editor1" name="text" rows="8"></textarea>
    </div>

	<div class="form-group">
	    <label for="tag">Tag *Setiap Tag pisahkan dengan tanda koma (,)*</label>
	    <textarea class="form-control" name="tag"  rows="2" required></textarea>
    </div>

    <div class="form-group">
	    <label for="news_image">Picture</label>
	    <input class="form-control-file" type="file" name="news_image" />
    </div>
    <input type="submit" name="submit" value="Submit News" class="btn btn-primary"/>
</form>