<div class="card-header">
<h4>Contact</h4>
</div>
<div class="card-body">
<?php echo form_open('news/contact'); ?>
<b><?php echo validation_errors(); ?></b>
<div class="row">	
	<div class="form-group col-md-3">
	    <label for="title">Name</label>
	    <input class="form-control" type="input" name="nama" />
   	</div>

   	<div class="form-group col-md-3">
	    <label for="title">Email</label>
	    <input class="form-control" type="input" name="email" />
   	</div>
</div>

<div class="row">	
   	<div class="form-group col-md-6">
	    <label for="text">Message</label>
	    <textarea class="form-control" name="message" rows="8"></textarea>
    </div>
</div>

<div class="row">	
	<div class="form-group col-md-6">
		<label for="text">Type the captcha :</label>
	    <input class="form-control" type="input" name="captcha" />
		<br />
		<p class="text-center"><?php echo $image; ?></p>  
    </div>
</div>
    <input type="submit" name="submit" value="Send Now" class="btn btn-primary"/>
</form>
