<div class="card-header">
<h4>All your featured article is stored here</h4>
</div>
<div class="card-body">
<?php
if($total_article == 0) {
	echo "<p>You don't have any article yet</p>";
}else{
?>
<table id="example" class="table table-bordered" style="width:100%">
<thead>
<tr>
	<th>No</th>
	<th style="width:30%">Tittle</th>
	<th>Tag</th>
	<th style="width:16%">Submit Time</th>
	<th style="width:9.5%">Action</th>
</tr>
</thead>
<tbody>
<?php $no=1; foreach ($user_item as $news_item): ?>
<tr>
       	<td><?php echo $no; ?></td>
       	<td><?php echo $news_item['title'] ?></td>
        <td><?php echo $news_item['tag'] ?></td>
        <td><?php echo $news_item['submit_date'] ?></td>
        <td>
        <a href="<?php echo site_url('news/user_article_edit/'.$news_item['slug']); ?>" class="btn btn-info">E</a>
        <a href="<?php echo site_url('news/delete/'.$news_item['slug'].'/'.$news_item['id']); ?>" class="btn btn-danger">D</a></td>
</tr>
<?php $no++; endforeach; ?>
</tbody>
</table>
<?php } ?>