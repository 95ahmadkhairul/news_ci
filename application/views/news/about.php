<div class="card-header">
<h4>ABOUT US</h4>
</div>
<div class="card-body">
<p> By : Master Admin | 10-10-2019</p>
<p><span style="font-size:12pt"><strong><span style="color:black">NEWS BRO BRO</span></strong></span></p>

<p style="text-align:justify"><span style="font-size:11pt"><span style="font-size:12.0pt"><span style="color:black">News Bro Bro is An online newspaper, a stand-alone publication or as the online version of a printed periodical.</span></span></span></p>

<p style="list-style-type:none">​​​​​​​<span style="font-size:12pt"><span style="color:black">Going online created more opportunities for newspapers, such as competing with broadcast journalism in presenting breaking news in a more timely manner. The credibility and strong brand recognition of well established newspapers, and the close relationships they have with advertisers, are also seen by many in the newspaper industry as strengthening their chances of survival. The movement away from the printing process can also help decrease costs.</span></span></p>

<p style="list-style-type:none"><span style="font-size:11pt"><span style="font-size:12.0pt"><span style="color:black">Online newspapers, like printed newspapers, have legal restrictions regarding libel, privacy and copyright,[2] also apply to online publications in most countries as in the UK. Also, the UK Data Protection Act applies to online newspapers and news pages.[3] Up to 2014, the PCC ruled in the UK, but there was no clear distinction between authentic online newspapers and forums or blogs. In 2007, a ruling was passed to formally regulate UK-based online newspapers, news audio, and news video websites covering the responsibilities expected of them and to clear up what is, and what isn't, an online news publication.[4]</span></span></span></p>

<p style="list-style-type:none"><span style="font-size:11pt"><span style="font-size:12.0pt"><span style="color:black">News reporters are being taught to shoot video[5] and to write in the succinct manner necessary for internet news pages. Some newspapers have attempted to integrate the internet into every aspect of their operations, e.g., the writing of stories for both print and online, and classified advertisements appearing in both media, while other newspaper websites may be quite different from the corresponding printed newspaper.</span></span></span></p>
