<?php
class News_model extends CI_Model {

    public function __construct()
        {
                $this->load->database();
        }
        
    public function get_news($slug = FALSE){
        if ($slug === FALSE)
        {
            $query = $this->db->order_by('id', 'DESC');
            $query = $this->db->get('news');
            return $query->result_array();
        } 
        //$query = $this->db->get_where('news', array('slug' => $slug));
        $query = $this->db->select('*');
		$query = $this->db->from('news');
		$query = $this->db->where('slug', $slug);
		$query = $this->db->join('user', 'news.author = user.username');
		$query = $this->db->get();
        return $query->row_array();
    }

    public function upload(){
    	$config['upload_path'] = './upload/';
    	$config['allowed_types'] = 'jpg|png|jpeg';
    	$config['max_size']  = '2048';
    	$config['remove_space'] = TRUE;
  
    	$this->load->library('upload', $config); // Load konfigurasi uploadnya
    	if($this->upload->do_upload('news_image')){ // Lakukan upload dan Cek jika proses upload berhasil
      	// Jika berhasil :
      		$return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
      		return $return;
    	}else{
      	// Jika gagal :
      		$return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
      		return $return;
    	}
    }
  
 	// Fungsi untuk menyimpan data ke database
  	public function set_news($upload){
	    $slag = htmlentities($this->input->post('title'));
		$sleg = url_title($slag, 'dash', TRUE);
	    $submit_date = date("Y-m-d H:i:s");
	    $slug = $sleg."-".strtotime($submit_date);

    	$data = array(
        	'title' => htmlentities($this->input->post('title')),
        	'slug' => $slug,
    	   	'text' => $this->input->post('text'),
     	  	'author' => $this->input->post('author'),
     	   'tag' => htmlentities($this->input->post('tag')),
     	   'submit_date' => $submit_date,
     	   'image' => $this->upload->data('file_name')       
    	);
		return $this->db->insert('news', $data);
    }

    public function set_message(){
    $data = array(
        'contact' => htmlentities($this->input->post('nama')),
        'email' => htmlentities($this->input->post('email')),
        'message' => htmlentities($this->input->post('message')),
        'time' => date("Y-m-d H:i:s")       
    );
		return $this->db->insert('contact', $data);
    }
        
    public function delete_news($id){
	    $row = $this->db->where('id', $id)->get('news')->row_array();
		if(unlink("./upload/".$row['image'])){
		    $this->db->where('id', $id);
		    return $this->db->delete('news');
	    }
    }

    public function delete_message($id){
		$this->db->where('id', $id);
		return $this->db->delete('contact');
    }

           
    public function edit(){         
        $slag = htmlentities($this->input->post('title'));
		$sleg = url_title($slag, 'dash', TRUE);
        $submit_date = date("Y-m-d H:i:s");
        $slug = $sleg."-".strtotime($submit_date);
        $id = $this->input->post('id');
        	
        if($_FILES['news_image']['name'] == NULL){
        	$image_name = $this->input->post('old_images');
        }else{
        	$image_name = $_FILES['news_image']['name'];
        }

        $data = array(
        	'title' => htmlentities($this->input->post('title')),
        	'slug' => $slug,
       		'text' => $this->input->post('text'),
       		'author' => $this->input->post('author'),
       		'tag' => htmlentities($this->input->post('tag')),
       		'submit_date' => $submit_date,
       		'image' => $image_name
       	);

        //$this->db->html_escape($data);                            
        $this->db->where('id', $id);
        return $this->db->update('news', $data);
    }
        
    public function slide_show_get(){	
    	$query = $this->db->order_by('id', 'DESC');
  	 	$query = $this->db->select('title, image, slug');
  	 	$query = $this->db->get('news', 5, 0);
  		return $query->result_array();
    }

  	public function get_data($rowno,$rowperpage,$search="") {
    $this->db->select('*');
    $this->db->from('news');

    if($search != ''){
    	if($_SESSION['type'] == 'search'){
			$this->db->like('title',$search);
			$this->db->or_like('text',$search);
			$this->db->or_like('author',$search);
			$this->db->or_like('tag',$search);
		}else{
			$this->db->like('tag',$search);
		}
    }

    $this->db->limit($rowperpage, $rowno); 
    $query = $this->db->get();
    return $query->result_array();
  	}

  	// Select total records
  	public function get_record_count($search = '') {
  	$this->db->escape($search); 
    $this->db->select('count(*) as allcount');
    $this->db->from('news');
    
    if($search != ''){
    	if($_SESSION['type'] == 'search'){
			$this->db->like('title',$search);
			$this->db->or_like('text',$search);
			$this->db->or_like('author',$search);
			$this->db->or_like('tag',$search);
		}else{
			$this->db->like('tag',$search);
		}
    }
    
    $query = $this->db->get();
    $result = $query->result_array();
    return $result[0]['allcount'];
  	}

  	public function account_creation(){
        $data = array(
	        'nama' => $this->input->post('name'),
	        'username' => $this->input->post('username'),
	        'password' => crypt($this->input->post('password'),"anti hack"),
	        'role' => "user",
	        'status' => "alive"   
        );
		return $this->db->insert('user', $data);
  	}
  
  	public function edit_user(){         
		if ($this->input->post('newpassword') != NULL){
			$password = crypt($this->input->post('newpassword'), "anti hack");
		}else{
			$password = crypt($this->input->post('password'), "anti hack");
		}

        $data = array(
        	'nama' => $this->input->post('nama'),
        	'password' => $password
        );

	    $this->db->where('id', $this->input->post('id'));
    	return $this->db->update('user', $data);
    }

    public function check_login($table, $where){		
		return $this->db->get_where($table,$where);
	}

	public function get_news_author($username){
	    $this->db->select('*');
		$this->db->from('user');
		$this->db->where('username', $username);
		$this->db->join('news', 'news.author = user.username');
		return $this->db->get();
    }

    public function check_username(){
    	$username = $this->input->post('username');
        return $this->db->get_where('user', array('username'=>$username))->num_rows();
    }

    public function get_all_news(){
        return $this->db->get('news');
    }

    public function get_all_user(){
        return $this->db->get_where('user', array('role'=>'user'));
    }

    public function get_user($username){
        return $this->db->get_where('user', array('username'=>$username));

    }

    public function banned($username){                           
        $this->db->where('username', $username);
        return $this->db->update('user', array('status'=>'Banned'));
    }

    public function unbanned($username){                           
        $this->db->where('username', $username);
        return $this->db->update('user', array('status'=>'Alive'));
    }

    public function popular_news($title){
    	$this->db->select('title, image, slug');
    	$this->db->order_by('id', 'DESC');
    	$this->db->like('title',$title);
    	return $this->db->get('news', 3, 0)->result_array();
    }

    public function popular(){
	    return $this->db->select('tag')->get('news')->result_array();
    }

    public function temp_tag($str){
        $this->db->truncate('temp_tag');
		return $this->db->insert_batch('temp_tag', $str);
  	}

  	public function temp_popular(){
  		$query = $this->db->query("SELECT all_tag, 
  			Count(*) AS jumlah 
  			FROM temp_tag 
  			GROUP BY all_tag 
  			ORDER BY jumlah DESC
  			LIMIT 0,5");

  		return $query->result_array();
  	}

  	public function get_contact(){
  		return $this->db->get('contact')->result_array();
  	}
}