<div class="card-header">
<h3><?php echo $title; ?></h3>
</div>
<div class="card-body">
<a href="<?php echo site_url('news/') ?>" class="btn btn-primary">Back</a>
<br />
<br />

<?php echo validation_errors(); ?>

<?php echo form_open_multipart('news/edit/'.$slug); ?>

    <label for="title">News Title</label>
    <input type="hidden" name="id" value="<?php echo $id;?>" />
    <input class="form-control" type="input" name="title" value="<?php echo $title; ?>" /><br />

    <label for="text">News Text</label>
    <textarea name="text" class="form-control" rows="4"><?php echo $text;?></textarea><br />

    <label for="author">Author</label>
    <input class="form-control" type="input" name="author" value="<?php echo $author;?>" /><br />

    <label for="tag">Tag</label>
    <textarea class="form-control" name="tag"  rows="4"><?php echo $tag;?></textarea><br />
    
    <div class="form-group">
    <label for="news_image">Picture</label>
    <input class="form-control-file" type="file" name="news_image" />
    </div>

    <label for="current_image">Current Picture :</label><br />
    <img class="current_image image-news-view" src="<?php echo base_url();?>upload/<?php echo $image;?>"><br /><br />
    <input type="hidden" value="<?php echo $image;?>" name="old_images">
    <input type="submit" name="submit" value="Edit News" class="btn btn-primary"/>
</form>