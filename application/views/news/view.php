<div class="card-header">
	<img class="image-news-view" src="<?php echo base_url();?>upload/<?php echo $news_item['image'];?>">
</div>
<div class="card-body">
	<h3><?php echo $title; ?></h3>
	<p> By : <?php echo $news_item['nama']; ?> | <?php echo date("d-m-Y", strtotime($news_item['submit_date'])); ?></p>
	<p><?php echo $news_item['text']; ?></p>
	<table>
		<tr>
		<?php $tag = explode(', ', $news_item['tag']); 
		foreach ($tag as $tags){
		echo form_open('news/archive'); ?>
			<td><input type="hidden" name="search_tag" value="<?php echo $tags;?>" />
			<input type="submit" name="submit" class="btn btn-info" value="<?php echo $tags;?>" />&nbsp;</td>
		</form>
		<?php } ?>
		</tr>
	</table>
<?php echo form_open('news/view/'.$news_item['slug']); ?>
<b><?php echo validation_errors(); ?></b>
<div class="row">	
	<div class="form-group col-md-3">
	    <label for="title">Name</label>
	    <input class="form-control" type="input" name="nama" />
   	</div>

   	<div class="form-group col-md-3">
	    <label for="title">Email</label>
	    <input class="form-control" type="input" name="email" />
   	</div>
</div>

<div class="row">	
   	<div class="form-group col-md-6">
	    <label for="text">Message</label>
	    <textarea class="form-control" name="message" rows="8"></textarea>
    </div>
</div>

<div class="row">	
	<div class="form-group col-md-6">
		<label for="text">Type the captcha :</label>
	    <input class="form-control" type="input" name="captcha" />
		<br />
		<p class="text-center"><?php echo $image; ?></p>  
    </div>
</div>
    <input type="submit" name="submit" value="Send Now" class="btn btn-primary"/>
</form>