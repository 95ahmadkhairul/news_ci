<?php if($user_item['role'] == "admin") { ?>
<div class="card-header">
	<h4>User Data Management Section</h4>
</div>
<div class="card-body">
	<table class="display table table-bordered" style="width:100%">
	<thead>
	<tr>
		<th>No</th>
		<th>Username</th>
		<th>Name</th>
		<th>Status</th>
		<th>Action</th>
	</tr>
	</thead>
	<tbody>
	<?php $no=1; foreach ($all_user as $all_user): ?>
	<tr>
       	<td><?php echo $no; ?></td>
       	<td><?php echo $all_user['username'] ?></td>
        <td><?php echo $all_user['nama'] ?></td>
        <td><?php echo $all_user['status'] ?></td>
        <td>
        <?php if($all_user['status'] == "Alive"){?>
       		<a href="<?php echo site_url('news/banned/'.$all_user['username']); ?>" class="btn btn-danger">Banned</a>
        <?php } else {?>
        	<a href="<?php echo site_url('news/banned/'.$all_user['username']); ?>" class="btn btn-info">Unbanned</a>
        <?php } ?>
        </td>
	</tr>
	<?php $no++; endforeach; ?>
	</tbody>
</table>
</div>
<div class="card-footer">
</div>
</div>
<br />
<div class="card">
<div class="card-header">
	<h4>Contact</h4>
</div>
<div class="card-body">
	<table class="display table table-bordered" style="width:100%">
	<thead>
		<tr>
			<th>No</th>
			<th>Contact</th>
			<th>E-mail</th>
			<th>Message</th>
			<th>Date Time</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		<?php $no=1; foreach ($contact as $con): ?>
		<tr>
		  	<td><?php echo $no; ?></td>
		   	<td><?php echo $con['contact'] ?></td>
		    <td><?php echo $con['email'] ?></td>
	        <td><?php echo $con['message'] ?></td>
	        <td><?php echo $con['time'] ?></td>
	        <td><a href="<?php echo site_url('news/delete_message/'.$con['id']); ?>" class="btn btn-danger">D</a></td>
		</tr>
		<?php $no++; endforeach; ?>
	</tbody>
	</table>
</div>
<div class="card-footer">
</div>
</div>
<br />
<div class="card">
<?php } ?>
<div class="card-header">
	<h4>Here's your profile</h4>
</div>
<div class="card-body">
<p>You can change <b>Change your account data</b> but only password and name not your username 
<b><?php echo $user_item['username']; ?></b>. Simply left New Password empty if u want to change your Name only but your Old Password (your current password) is still required</p>
<?php echo form_open('news/user_profile'); ?>
<div class="row">
<div class="col-md-4 border_user">	
	<input type="hidden" name="id" value="<?php echo $user_item['id'];?>" />
   	<div class="form-group">
	    <label for="nama">Name</label>
	    <input class="form-control" type="input" name="nama" value="<?php echo $user_item['nama'];?>" required />
    </div>

   	<div class="form-group">
	    <label for="newpassword">New Password</label>
	    <input class="form-control" type="Password" name="newpassword" />
    </div>

	<div class="form-group">
	    <label for="password">Old password</label>
	    <input class="form-control" type="Password" name="password" required/>
    </div>

    <input type="submit" name="submit" value="Update" class="btn btn-primary"/>
</form>
</div>
<div class="col-md-8">	
	<?php echo validation_errors();
	echo $validation; ?>
</div>
</div>
