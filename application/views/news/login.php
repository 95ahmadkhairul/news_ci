<div class="card-header">
</div>
<div class="card-body">
<div class="row">
<div class="col-sm-5 border_user">
<p><h5 class="text-center">Log in</h5></p>
<?php echo form_open('news/admin', 'class="my-2 my-lg-0"'); ?>
	 <div class="form-group">
        <label for="username">Username</label>
        <input type="text" class="form-control" name="username" placeholder="Username" required autofocus>
    </div>
    <div class="form-group">
        <label for="password">Password</label>
        <input type="password" class="form-control" name="password" placeholder="Password" required>
    </div>
    <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
</form>
</div>
<div class="col-sm-2 border_user">
	<h4 class="text-center align-middle"><br /><br />OR</h4>
</div>
<div class="col-sm-5">
<p><h5 class="text-center">Don't Have Account? Create Now !!</h5></p>
<?php echo form_open('news/sign_up', 'class="my-2 my-lg-0"'); ?>
	<div class="form-group">
        <label for="name">Name</label>
        <input type="text" class="form-control" name="name" />
    </div>
	 <div class="form-group">
        <label for="username">Username</label>
        <input type="text" class="form-control" name="username" />
    </div>
    <div class="form-group">
        <label for="password">Password</label>
        <input type="password" class="form-control" name="password" />
    </div>
    <button class="btn btn-lg btn-primary btn-block" type="submit">Sign up</button>
</form>
<p><?php echo validation_errors(); ?><p>
<p><?php echo $validation; ?><p>
</div>
</div>