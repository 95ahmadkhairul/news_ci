<div class="card-header">
<h3><?php echo $title; ?></h3>
</div>
<div class="card-body">
<div class="row">
<div class="col-md-12 text-center">
	<h4>LATEST NEWS</h4><br />
</div>
<?php foreach ($news_item as $news) { ?>
	<div class="col-md-4">
	<p><img src="<?php echo base_url();?>upload/<?php echo $news['image'];?>" width="350" height="250"/></p>
	<p><a href="<?php echo site_url('news/view/'.$news['slug']); ?>"><?php echo $news['title'];?></a></p>
	</div>
<?php } ?>
<div class="col-md-12 text-center">
	<h4>POPULAR NEWS By CATEGORY</h4><br />
</div>
<?php for ($x=0; $x < 3; $x++){ ?>
	<div class="col-md-4">
		<h4><?php echo $popular[$x]['all_tag']; ?></h4><br />
	<?php foreach ($pop[$x] as $pops) { ?>		
		<p><img src="<?php echo base_url();?>upload/<?php echo $pops['image'];?>" width="350" height="150"/></p>
		<p><a href="<?php echo site_url('news/view/'.$pops['slug']); ?>"> <?php echo $pops['title'];?></a></p>
	<?php } ?>
	</div>
<?php }
//echo $popular[0]['all_tag']; 
?>
</div>