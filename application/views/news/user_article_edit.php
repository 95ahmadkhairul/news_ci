<div class="card-header">
<h4>Edit News : <?php echo $title; ?></h4> 
</div>
<div class="card-body">
<?php echo validation_errors(); ?>
<div style="color: red;"><?php echo (isset($message))? $message : ""; ?></div>
<?php echo form_open_multipart('news/user_article_edit/'.$slug); ?>
	<input type="hidden" name="author" value="<?php echo $author;?>" />
	<input type="hidden" name="id" value="<?php echo $id;?>" />
	<input type="hidden" value="<?php echo $image;?>" name="old_images">

	<div class="form-group">
	    <label for="title">News Title</label>
	    <input class="form-control" type="input" name="title" value="<?php echo $title; ?>" required/>
	</div>

	<div class="form-group">
	    <label for="text">News Text</label>
	     <textarea id="editor1" name="text" rows="8"><?php echo htmlentities($text);?></textarea>
	</div>
    
    <div class="form-group">
	    <label for="tag">Tag *Setiap Tag pisahkan dengan tanda koma (,)*</label>
	    <textarea class="form-control" name="tag" rows="2" required><?php echo $tag;?></textarea>
    </div>

    <div class="form-group">
	    <label for="news_image">Picture</label>
	    <input class="form-control-file" type="file" name="news_image" />
    </div>

    <label for="current_image">Current Picture :</label><br />
    <img class="current_image image-news-view" src="<?php echo base_url();?>upload/<?php echo $image;?>">
    <br /><br />
    <input type="submit" name="submit" value="Edit News" class="btn btn-primary"/>
</form>