<div class="card-header">
<h3><?php echo $title; ?></h3>
</div>
<div class="card-body">
<a href="<?php echo site_url('news/') ?>" class="btn btn-primary">View News</a>
<br />
<br />

<?php echo validation_errors(); ?>
<div style="color: red;"><?php echo (isset($message))? $message : ""; ?></div>
<?php echo form_open_multipart('news/create'); ?>

    <label for="title">News Title</label>
    <input class="form-control" type="input" name="title" required/><br />
   
    <label for="text">News Text</label>
    <textarea name="text" class="form-control" rows="4" required></textarea><br />

    <label for="author">Author</label>
    <input class="form-control" type="input" name="author" required/><br />

    <label for="tag">Tag</label>
    <textarea class="form-control" name="tag"  rows="4"required></textarea><br />
    
    <div class="form-group">
    <label for="news_image">Picture</label>
    <input class="form-control-file" type="file" name="news_image" />
    </div>

    <input type="submit" name="submit" value="Submit News" class="btn btn-primary"/>

</form>