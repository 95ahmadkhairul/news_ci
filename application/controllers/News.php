<?php
class News extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('news_model');
        $this->load->helper('url_helper');    
        $this->load->helper('date');  
		$this->load->library('session');    
		$this->load->library('pagination');    
		$this->load->library('form_validation');
		$this->load->helper(array('form', 'url','captcha'));
    	$this->load->library('form_validation');

    }

    public function index(){
	//Popular Tags
	$data['tags_arr'] = $this->news_model->popular();
	$string = "";

	//change array from per article tags to per tags
	foreach ($data['tags_arr'] as $tags) {
 		$string .= $tags['tag'].", ";
	} 
	
	$str_trm = trim($string, " ");
	$str_arr = explode(',', $str_trm);
	$str = array();
	
	foreach ($str_arr as $tags) {
 		$str[] = array('all_tag' => $tags);
	} 

	//insert new array to temporary table
	$this->news_model->temp_tag($str);
	$data['popular'] = $this->news_model->temp_popular();
    
    //popular tag
    $title = $data['popular'][0]['all_tag'];
	$data['pop'][0] = $this->news_model->popular_news($title);
    $title = $data['popular'][1]['all_tag'];
	$data['pop'][1] = $this->news_model->popular_news($title);
    $title = $data['popular'][2]['all_tag'];
	$data['pop'][2] = $this->news_model->popular_news($title);
    //load view 
    $data['title'] = "Home";
    $slide_show['news'] = $this->news_model->slide_show_get();
	$data['news_item'] = $this->db->order_by('id', 'DESC')->get('news', 3, 0)->result_array();
	//print_r($data['pop']);
    $this->load->view('templates/header', $slide_show);
    $this->load->view('news/index', $data);
    $this->load->view('templates/footer');   
  	}
    
  	public function archive($rowno=0){
    $data['title'] = 'News Archive';
    // Search text
    $search_text = "";
    
    if($this->input->post('submit') != NULL){
    	if($this->input->post('search') != NULL){
    		$search_text = $this->input->post('search');
      		$this->session->set_userdata(array("search"=>$search_text, "type"=>"search"));
     		$data['title'] = 'Search Result';	
    	}else if($this->input->post('search_tag') != NULL){
    		$search_text = $this->input->post('search_tag');
      		$this->session->set_userdata(array("search"=>$search_text, "type"=>"search_tag"));
     		$data['title'] = 'Tag Search Result';
    	}
    }else{
    	if($this->session->userdata('search') != NULL){
        $search_text = $this->session->userdata('search');
        $data['title'] = 'Search Result';
      }
    }

    /*
    if($this->input->post('submit') != NULL ){
      $search_text = $this->input->post('search');
      $this->session->set_userdata(array("search"=>$search_text));
      $data['title'] = 'Search Result';
    }else{
      if($this->session->userdata('search') != NULL){
        $search_text = $this->session->userdata('search');
        $data['title'] = 'Search Result';
      }
    }*/

    $search_text = htmlentities($search_text);
    // Row per page
    $rowperpage = 5;

    // Row position
    if($rowno != 0){
      $rowno = ($rowno-1) * $rowperpage;
    }
 
    // All records count
    $allcount = $this->news_model->get_record_count($search_text);

    // Get records
    $users_record = $this->news_model->get_data($rowno,$rowperpage,$search_text);
 
    // Pagination Configuration
    $config['base_url'] = base_url().'news/archive';
    $config['use_page_numbers'] = TRUE;
    $config['total_rows'] = $allcount;
    $config['per_page'] = $rowperpage;

	// Style
	$config['first_link']       = 'First';
	$config['last_link']		= 'Last';
	$config['next_link']		= 'Next';
	$config['prev_link']		= 'Prev';
	$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
	$config['full_tag_close']   = '</ul></nav></div>';
	$config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
	$config['num_tag_close']    = '</span></li>';
	$config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
	$config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
	$config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
	$config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
	$config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
	$config['prev_tagl_close']  = '</span>Next</li>';
	$config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
	$config['first_tagl_close'] = '</span></li>';
	$config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
	$config['last_tagl_close']  = '</span></li>';

    // Initialize
    $this->pagination->initialize($config);
 
    $data['pagination'] = $this->pagination->create_links();
    $data['result'] = $users_record;
    $data['row'] = $rowno;
    $data['search'] = $search_text;
    $data['allcount'] = $allcount;

    //Popular Tags
	$data['tags_arr'] = $this->news_model->popular();
	
	$string = "";
	//change array from per article tags to per tags
	foreach ($data['tags_arr'] as $tags) {
 		$string .= $tags['tag'].", ";
	} 
	$str_trm = str_replace(" ", "", $string);
	$str_arr = explode(',', $str_trm);
	$str = array();
	foreach ($str_arr as $tags) {
 		$str[] = array('all_tag' => $tags);
	} 
	
	//insert new array to temporary table
	$this->news_model->temp_tag($str);
	$data['popular'] = $this->news_model->temp_popular();
    
    // Load view 
    $slide_show['news'] = $this->news_model->slide_show_get();
	
    $this->load->view('templates/header', $slide_show);
    $this->load->view('news/archive', $data);
    $this->load->view('templates/footer');   
  	}
    
    public function contact(){ 
    $slide_show['news'] = $this->news_model->slide_show_get();
    $this->session->set_userdata('image', 'images');

    $this->form_validation->set_rules('nama', 'Name', 'required|alpha_numeric');
    $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
    $this->form_validation->set_rules('message', 'Message', 'required|alpha_numeric_spaces');
    $this->form_validation->set_rules('captcha', 'Captcha', 'required|callback_validate_captcha');
	
	if($this->form_validation->run() == FALSE){
        
        if(file_exists("./captcha/".$this->session->userdata['image']))
            unlink("./captcha/".$this->session->userdata['image']);

        $original_string = array_merge(range(0,9), range('a','z'), range('A', 'Z'));
	    $original_string = implode("", $original_string);
	    $captcha = substr(str_shuffle($original_string), 0, 6);
		//Field validation failed.  User redirected to login page
	    $vals = array(
	        'word' => $captcha,
	        'img_path' => './captcha/',
	        'img_url' => base_url('captcha'),
	        'font_path' => './captcha/times-new-roman.ttf',
	        'img_width' => 300,
	        'img_height' => 80,
	        'font_size' => 40,
	        'expiration' => 7200,
	        'colors' => array(
	            'background' => array(255, 255, 255),
	            'border' => array(0, 0, 0),
	            'text' => array(0, 0, 0),
	            'grid' => array(255, 40, 40)
	 		)
	    );

	    $cap = create_captcha($vals);
	    $data['image'] = $cap['image'];

        $this->session->set_userdata(array('captcha'=>$captcha, 'image' => $cap['time'].'.jpg'));
        
        $this->load->view('templates/header', $slide_show);
    	$this->load->view('news/contact', $data);
    	$this->load->view('templates/footer');  
        
    }else{
        if(file_exists("./captcha/".$this->session->userdata['image']))
            unlink("./captcha/".$this->session->userdata['image']);

        $this->news_model->set_message();
		echo "<script>alert('Message sent');</script>";
		redirect('news/contact', 'refresh');
    }
	}

	public function validate_captcha(){
    if($this->input->post('captcha') != $this->session->userdata['captcha']){
        $this->form_validation->set_message('validate_captcha', 'Wrong Captcha');
        return false;
    }else{
        return true;
    }
	}

	public function about(){
    $slide_show['news'] = $this->news_model->slide_show_get();
	//print_r($data['pop']);
    $this->load->view('templates/header', $slide_show);
    $this->load->view('news/about');
    $this->load->view('templates/footer');   
  	}

	public function view($slug = NULL){
	$data['news_item'] = $this->news_model->get_news($slug);

	if (empty($data['news_item']))
	{
	show_404();
	}

	$slide_show['news'] = $this->news_model->slide_show_get();
	$data['title'] = $data['news_item']['title'];

	$this->load->view('templates/header', $slide_show);
	$this->load->view('news/view', $data);
	$this->load->view('templates/footer');
	}
	
	public function login()
	{
	$data['validation'] = "";
	if(isset($_SESSION['nama'])){
		redirect("news/user_article");
	}else{
		$data['title'] = "";
		$slide_show['news'] = $this->news_model->slide_show_get();
		$this->load->view('templates/header', $slide_show);
		$this->load->view('news/login', $data);
		$this->load->view('templates/footer');
	}
	}

	public function home(){
    	unset($_SESSION['search']);
    	redirect('news/archive'); 
  	}

  	public function admin(){
  		$username = $this->input->post('username');
		$password = crypt($this->input->post('password'),"anti hack");
		$data['news_total'] = $this->news_model->get_all_news()->num_rows();

		if(isset($_SESSION['nama'])){
			$username = $_SESSION['nama'];
			$data['user_item'] = $this->news_model->get_news_author($username)->result_array();
			$data['total_article'] = $this->news_model->get_news_author($username)->num_rows();
			
			$this->load->view('templates/header_user');
			$this->load->view('news/admin', $data);
			$this->load->view('templates/footer');

		}else if($username != NULL && $password != NULL){
			$data['user_item'] = $this->news_model->get_news_author($username)->result_array();
			$data['total_article'] = $this->news_model->get_news_author($username)->num_rows();	
			$where = array(
				'username' => $username,
				'password' => $password
			);
			$check = $this->news_model->check_login("user",$where)->num_rows();
			if($check > 0){							
				$data['check_item'] = $this->news_model->check_login("user",$where)->row_array();
				
				if($data['check_item']['status'] == "Banned"){
					echo "<script>alert('Your account had been suspended');</script>";
					redirect('news/login', 'refresh');

				}else{
					$this->session->set_userdata('nama', $username);
					//var_dump($data);
					$this->load->view('templates/header_user');
					$this->load->view('news/user_article', $data);
					$this->load->view('templates/footer');

				}

			}else{
				echo "<script>alert('Username or Password is wrong!!');</script>";
				redirect('news/login', 'refresh');

			}
		}else{
			echo "<script>alert('You need to login first');</script>";
			redirect('news/login', 'refresh');
		}
	}	

	public function user_article(){
		if(isset($_SESSION['nama'])){
			$username = $_SESSION['nama'];
			$data['user_item'] = $this->news_model->get_news_author($username)->result_array();
			$data['total_article'] = $this->news_model->get_news_author($username)->num_rows();
			
			$this->load->view('templates/header_user');
			$this->load->view('news/user_article', $data);
			$this->load->view('templates/footer');
		}else{		
			echo "<script>alert('You need to login first');</script>";
			redirect('news/login', 'refresh');
		}
	}

	public function user_profile(){
		if(isset($_SESSION['nama'])){
			$data['validation'] = "";
			$where = array('username' => $_SESSION['nama']);
			$data['user_item'] = $this->news_model->check_login("user",$where)->row_array();
			$author = $data['user_item']['nama'];
			$data['all_user'] = $this->news_model->get_all_user()->result_array();
			$data['contact'] = $this->news_model->get_contact();
			
			//var_dump($data);

			$this->form_validation->set_rules('nama', 'Name', 'required|max_length[15]|alpha_numeric_spaces');
			$this->form_validation->set_rules('password', 'Password', 'required|min_length[6]');

			if ($this->input->post('newpassword') != NULL){
			$this->form_validation->set_rules('newpassword', 'New Password', 'required|min_length[6]');
			}

			if ($this->form_validation->run() === FALSE){		
				$this->load->view('templates/header_user');
				$this->load->view('news/user_profile', $data);
				$this->load->view('templates/footer');
			}else{	
				$where = array('password' => crypt($this->input->post('password'),"anti hack"));
				$pass_check = $this->news_model->check_login("user",$where)->row_array();
				
				if($pass_check < 1){
					$data['validation'] = "Your Password is Wrong";
				}else{
					$this->news_model->edit_user();
					$data['validation'] = "Your Account Data Successfully Updated";
				}

				$this->load->view('templates/header_user');
				$this->load->view('news/user_profile', $data);
				$this->load->view('templates/footer');
			}
		}else{		
			echo "<script>alert('You need to login first');</script>";
			redirect('news/login', 'refresh');
		}
	}

	public function user_create(){
		if(isset($_SESSION['nama'])){
			$data['title'] = 'Create News';
			$where = array('username' => $_SESSION['nama']);
			$data['user_item'] = $this->news_model->check_login("user",$where)->row_array();

			$this->form_validation->set_rules('title', 'Title', 'required');
			$this->form_validation->set_rules('text', 'Text', 'required');
			$this->form_validation->set_rules('tag', 'Tag', 'required|alpha_numeric');

			if ($this->form_validation->run() === FALSE){
				$this->load->view('templates/header_user');
				$this->load->view('news/user_create', $data);
				$this->load->view('templates/footer');
			} else {   
				$extension = explode(".", $_FILES['news_image']['name']);
				$_FILES['news_image']['name'] = strtotime(date("Y-m-d h:i:sa"))+rand(1,10000).".".end($extension);
				$upload = $this->news_model->upload();  
				
				if($upload['result'] == "success") { 
					$this->news_model->set_news($upload);
					echo "<script>alert('New Article Successfully Added');</script>";
					redirect('news/user_article', 'refresh'); 		
				} else { 
					$data['message'] = $upload['error'];
					$this->load->view('templates/header_user');
					$this->load->view('news/user_create', $data);
					$this->load->view('templates/footer');
				}	
			}
		}else{		
			echo "<script>alert('You need to login first');</script>";
			redirect('news/login', 'refresh');
		}
	}

	public function delete($slug,$id){
	if(isset($_SESSION['nama'])){
		$data['news_item'] = $this->news_model->get_news($slug);
		if ($data['news_item']['author'] != $_SESSION['nama']){
			show_404(); 
		}

		$id = $this->uri->segment(4);
		if (empty($id)){
			show_404();
		}

		$delete = $this->news_model->delete_news($id);
		echo "<script>alert('News Successfully Deleted');</script>";
		redirect('news/user_article', 'refresh');	
	}else{
		echo "<script>alert('You need to login first');</script>";
		redirect('news/login', 'refresh');
	}
	}

	public function delete_message($id){
	if(isset($_SESSION['nama'])){
		$data['user_item'] = $this->news_model->get_user($_SESSION['nama'])->result_array();
		if ($data['user_item'][0]['role'] != "admin") {
			show_404();
		}

		$id = $this->uri->segment(3);
		if (empty($id)){
			show_404();
		}

		$delete = $this->news_model->delete_message($id);
		echo "<script>alert('Message Successfully Deleted');</script>";
		redirect('news/user_profile', 'refresh');	
	}else{
		echo "<script>alert('You need to login first');</script>";
		redirect('news/login', 'refresh');
	}
	}

	public function logout(){
    	unset($_SESSION['nama']);
    	redirect('news/login'); 
  	}

  	public function user_article_edit($slug){
	if(isset($_SESSION['nama'])){
		$data['news_item'] = $this->news_model->get_news($slug);

		if ($data['news_item']['author'] != $_SESSION['nama']){
			show_404(); 
		}		
		
		if(empty($data['news_item'])){ 
			show_404(); 
		}

		$data = $this->news_model->get_news($slug);
		
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('text', 'Text', 'required');
		$this->form_validation->set_rules('tag', 'Tag', 'required|alpha_numeric');

		if ($this->form_validation->run() === FALSE){
			$this->load->view('templates/header_user');
			$this->load->view('news/user_article_edit', $data);
			$this->load->view('templates/footer');

		}else{
			if (empty($_FILES['news_image']['name'])){
				$this->news_model->edit();
				echo "<script>alert('Edit data success');</script>";
				redirect('news/user_article', 'refresh');

			}else{
				$extension = explode(".", $_FILES['news_image']['name']);
				$_FILES['news_image']['name'] = strtotime(date("Y-m-d h:i:sa"))+rand(1,10000).".".end($extension);
				$upload = $this->news_model->upload();

				if($upload['result'] == "success"){ 
					$news_image_name = $this->input->post('old_images');

					if(unlink("./upload/".$news_image_name)){ 
						$this->news_model->edit();
						//$_FILES['news_image']['name'] = $upload['file']['file_name'];	
						echo "<script>alert('Edit data success');</script>";
						redirect('news/user_article', 'refresh');

					}else{
						echo "<script>alert('Delete Image Failed');</script>";
						redirect('news/user_article', 'refresh');

					}	
				}else{ 
					$data['message'] = $upload['error'];
					$this->load->view('templates/header_user');
					$this->load->view('news/user_article_edit', $data);
					$this->load->view('templates/footer');

			  	}
			}
		}  
	}else{
		echo "<script>alert('You need to login first');</script>";
		redirect('news/login', 'refresh');

	}
	}	

	public function banned($username){
	if(isset($_SESSION['nama'])){
		$where = array('username' => $_SESSION['nama']);
		$data['user_item'] = $this->news_model->check_login("user",$where)->row_array();
		
		if ($data['user_item']['role'] != "admin"){
			show_404(); 
		}

		$username = $this->uri->segment(3);
		if (empty($username)){
			show_404();
		}

		$data['banned'] = $this->news_model->check_login("user", array('username' => $username))->row_array();

		if ($data['banned']['status'] == "Alive"){
			$this->news_model->banned($username);
			echo "<script>alert('Account Banned Successfull');</script>";
			redirect('news/user_profile', 'refresh');	
		}else{
			$this->news_model->unbanned($username);
			echo "<script>alert('Account Unbanned Successfull');</script>";
			redirect('news/user_profile', 'refresh');	
		}
	}else{
		echo "<script>alert('You need to login as Admin first');</script>";
		redirect('news/login', 'refresh');
	}
	}

	public function sign_up(){
		$data['validation'] = "";
		$slide_show['news'] = $this->news_model->slide_show_get();
		
		$this->form_validation->set_rules('name', 'Name', 'required|max_length[15]|alpha_numeric_spaces');
		$this->form_validation->set_rules('username', 'Username', 'required|max_length[15]|alpha_numeric');
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[6]');
			
			if ($this->form_validation->run() === FALSE){		
				$this->load->view('templates/header', $slide_show);
				$this->load->view('news/login', $data);
				$this->load->view('templates/footer');	
			} else {   
				$user_check = $this->news_model->check_username();
				if ($user_check > 0){
					$data['validation'] = "Username Already Taken";
				}else{
					$this->news_model->account_creation();
					$data['validation'] = "Account Creation Succes, You can login now";	
				}

				$this->load->view('templates/header', $slide_show);
				$this->load->view('news/login', $data);
				$this->load->view('templates/footer');
			}	
	}	

	public function hash_me(){
		$word = $this->uri->segment(3);
		$hash = crypt($word,"anti hack");
		echo $word."<br />";
		echo strlen($hash)."<br />";
		echo $hash;
	}
}