<html>
        <head>
        <title>NEWS BRO BRO</title>
        <script src="<?php echo site_url();?>application/assets/jquery.min.js"></script>
        <link href="<?php echo site_url();?>application/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
        <link href="<?php echo site_url();?>application/assets/stylish.css" rel="stylesheet" />
        </head>
        <body>
        	<div class="container">
        <!-- Nav Bar -->
            <nav class="navbar navbar-expand-lg navbar navbar-light bg-light">
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                  <a href="<?php echo site_url('news/index')?>" class="nav-link">Home</a>
                </li>
                <li class="nav-item">
                  <a href="<?php echo site_url('news/home')?>" class="nav-link">Archive</a>
                </li>
                <li class="nav-item">
                  <a href="<?php echo site_url('news/contact')?>" class="nav-link">Contact</a>
                </li>
                <li class="nav-item">
                  <a href="<?php echo site_url('news/about')?>" class="nav-link">About Us</a> 
                </li>
              </ul>
              <?php echo form_open('news/archive', 'class="form-inline my-2 my-lg-0"'); ?>
                <input class="form-control mr-sm-2" type="search" name="search" value="<?php $search ?>" placeholder="Search" />
                <input type="submit" name="submit" value="Find" class="btn btn-outline-success my-2 my-sm-0" />
              </form>
              &nbsp;
              <a href="<?php echo site_url('news/login')?>" class="btn btn-info">Login</a></p>
            </div>
          </nav>
        <!-- End Nav Bar -->
        <!-- Slide Show -->
        	<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  				<ol class="carousel-indicators">
<?php $no =0; $actived=' class="active"'; foreach ($news as $news_item): ?>
					<li data-target="#carouselExampleIndicators" data-slide-to="<?php echo $no;?>"<?php echo $actived;?>></li>
<?php $no++; $actived =""; endforeach; $slide = array('First slide', 'Second slide', 'Third slide', 'Fourth slide', 'Fifth slide'); ?>
  				</ol>
  				<div class="carousel-inner">
<?php $no =0; $actived=" active"; foreach ($news as $news_item): ?>
					<div class="carousel-item<?php echo $actived;?>">
						<img class="image-news-view d-block w-100" 
						src="<?php echo base_url();?>upload/<?php echo $news_item['image'];?>" 
						alt="<?php echo $slide[$no];?>">
						<div class="carousel-caption d-none d-md-block">
							<h4><a class="font-link" href="<?php echo site_url('news/view/'.$news_item['slug']);?>">
							<?php echo $news_item['title'];?></a></h4>
						</div>
					</div>
<?php $no++; $actived =""; endforeach; ?>
    			</div>
  				<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    				<span class="carousel-control-prev-icon" aria-hidden="true"></span>
    				<span class="sr-only">Previous</span>
  				</a>
  				<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    				<span class="carousel-control-next-icon" aria-hidden="true"></span>
    				<span class="sr-only">Next</span>
  				</a>
			</div>
      <!-- End Slide Show -->
	  <div class="card">