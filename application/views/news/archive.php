<div class="card-header">
<h3><?php echo $title; ?></h3>
</div>
<div class="card-body">
<div class="row">
<div class="col-md-9">
<?php echo validation_errors();
if(count($result) == 0) {
	echo "<p>No article found for <b>".$search."</b> try different keyword <a href=".site_url('news/home').">back</a></p>";
} else if($search != '') {
	echo "<p>Found <b> ".$allcount."</b> articles based on your keyword <a href=".site_url('news/home').">back</a></p>";
}
foreach ($result as $news_item): ?>
        <a href="<?php echo site_url('news/view/'.$news_item['slug']); ?>"><h5>
        <?php echo strtoupper($news_item['title']) ?></h5></a>
        <div class="main">
        <p class="text-justify"><?php echo substr(strip_tags($news_item['text']), 0, 300)." ..." ?>
        <a href="<?php echo site_url('news/view/'.$news_item['slug']); ?>">[Read More]</a></p>
        </div>
<?php endforeach; 
echo $pagination;
?>
</div>
<div class="col-md-3">
<h5>POPULAR TAGS</h5>
<?php 
foreach ($popular as $tags) {
echo form_open('news/archive'); ?>
			<input type="hidden" name="search_tag" value="<?php echo $tags['all_tag'];?>" />
			<input type="submit" name="submit" class="btn btn-info" value="<?php echo $tags['all_tag'].' ';?>" />
</form>
<?php } ?>
</div>
</div>